﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Dynamic;
using Newtonsoft.Json;

namespace u18020659_INF354_HW1.Controllers
{
    public class ValuesController : ApiController
    {
        CarsDBEntities db = new CarsDBEntities();
        // GET api/values
        public IEnumerable<int> Get()
        {
            var years = db.CarYears.Select(zz => zz.Year).ToList();

            return years;
        }

        // GET api/values/5
        public List<dynamic> Get(int id)
        {
            

            var newcars = db.CarBrands.Where(zz => zz.ID == id).Include(zz=> zz.CarModels).ToList();
            List<dynamic> carinfo = new List<dynamic>();
            foreach (CarBrand brand in newcars)
            {
                dynamic expano = new ExpandoObject();
                expano.Brand = brand.BrandName;
                expano.Models = getmodels(brand);
                carinfo.Add(expano);
            }
            return carinfo;
        }

        private List<dynamic> getmodels(CarBrand brand)
        {
            List<dynamic> dynamicmodels = new List<dynamic>(); 
            foreach(CarModel mymodels in brand.CarModels)
            {
                dynamic model = new ExpandoObject();
                model.Name = mymodels.ModelName;
                model.ID = mymodels.ID;
                dynamicmodels.Add(model);
            }
            return dynamicmodels;
        }

        // POST api/values
        public void Post([FromBody]CarModel model)
        {
            db.CarModels.Add(model);
            db.SaveChanges();
        }

        // PUT api/values/5
        public void Put(int id, [FromBody] CarModel model)
        {
            CarModel oldmodel = db.CarModels.Where(zz => zz.ID == id).FirstOrDefault();
            oldmodel.ModelName = model.ModelName;
            db.SaveChanges();
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            CarModel newmodel = new CarModel();
            newmodel = db.CarModels.Where(zz => zz.ID == id).FirstOrDefault();
            db.CarModels.Remove(newmodel);
            db.SaveChanges();
        }
    }
}
